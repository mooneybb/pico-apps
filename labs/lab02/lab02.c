#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.


/**
 * @brief EXAMPLE - HELLO_C
 *        Simple example to initialise the IOs and then 
 *        print a "Hello World!" message to the console.
 * 
 * @return int  Application return code (zero for success).
 */


// calculates PI as a single-precision floating point representaion 
float floatPI(int limit) {
    float halfPI =1.0;
    for(int n=1; n<limit; n++){

        halfPI *= (4.0 * n * n) / ((4.0 * n * n) - 1);
    }
    return (2.00*halfPI);
}

// function which returns the precision to PI of a given float passsed as a parameter
float floatPrecision(float foundPI){
    float precision = ((foundPI - 3.14159265359)/3.14159265359 *100);
    if (precision < 0) return -1.0 * precision;
    return precision;
}

// which calculates PI as a double-precision floating point representation 
double doublePI(int limit) {
    double halfPI=1.0;
    for(int n=1; n<limit; n++){
        halfPI = halfPI * ((4.0*n*n) / ((4.0*n*n)-1.0));
    }
    return 2.0*halfPI;
}

// function which returns the prescion to PI of a given double passed as a parameter
double doublePrecision(double foundPI){
    double precision = ((foundPI - 3.14159265359)/3.14159265359 *100.0);
    if (precision < 0) return -1.0 * precision;
    return precision;
}



int main() {

// #ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    stdio_init_all();
// #endif

    // Print a console message to inform user what's going on.
    printf("Hello World!\n");

    // Print the single-precioson representation for PI
    printf("Single presion PI = %.10f \n",floatPI(100000));

    // print the calculated error for the single-precision representatio of PI
    printf("error aproximation = %.5f%%\n", floatPrecision(floatPI(100000)));

    // Print the double-precision representation for PI
    printf("Double precision PI = %.10lf \n", doublePI(100000));

    // Print the calculated error for the double-precision representation of PI
    printf("Error aproximation = %.5lf%%\n", doublePrecision(doublePI(100000)));

    // Returning zero indicates everything went okay.
    return 0;
}