#include "pico/stdlib.h"

/**
 * @brief EXAMPLE - BLINK_C
 *        Simple example to initialise the built-in LED on
 *        the Raspberry Pi Pico and then flash it forever. 
 * 
 * @return int  Application return code (zero for success).
 */
int main() {

    // Specify the PIN number and sleep delay
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;

    // Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    // Do forever...
    // I am assuming that only one subroutine is being looked for
    while (true) {
        // call blink subroutine
        blink(LED_PIN, LED_DELAY);

    }
    
    

    // Should never get here due to infinite while-loop.
    return 0;

}
// Subroutine blink which takes in two parameters: 1. the led pin number,
// 2. the led delay time, and toggles the led of the given pin (on and off) after the delay
void blink(const uint led_pin, const uint led_delay){
            // Toggle the LED on and then sleep for delay period
            gpio_put(led_pin, 1);
            sleep_ms(led_delay);

            // Toggle the LED off and then sleep for delay period
            gpio_put(led_pin, 0);
            sleep_ms(led_delay);
    }